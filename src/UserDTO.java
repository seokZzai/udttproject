package com.greedy.swings.model.dto;

public class UserDTO {

	private int userNO;
	private String userId;
	private String userPassword;
	private String userEmail;
	
	public UserDTO() {
		super();
	}
	
	public UserDTO(int userNO, String userId, String userPassword, String userEmail) {
		super();
		this.userNO = userNO;
		this.userId = userId;
		this.userPassword = userPassword;
		this.userEmail = userEmail;
	}
	
	public int getUserNO() {
		return userNO;
	}
	
	public void setUserNO(int userNO) {
		this.userNO = userNO;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserPassword() {
		return userPassword;
	}
	
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public String getUserEmail() {
		return userEmail;
	}
	
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	@Override
	public String toString() {
		return "UserDTO [userNO=" + userNO + ", userId=" + userId + ", userPassword=" + userPassword + ", userEmail="
				+ userEmail + "]";
	}
	
	
	
}
