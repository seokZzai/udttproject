import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginFrame extends JFrame {
	
	public LoginFrame() {
		
		this.setSize(1200, 800);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(0, 0, 1200, 800);
		panel1.setBackground(Color.WHITE);
		
		JLabel titleLabel1 = new JLabel("★우당탕탕★");
		titleLabel1.setBounds(800, 50, 250, 80);
		titleLabel1.setFont(titleLabel1.getFont().deriveFont(40.0f));
		
		JLabel titleLabel2 = new JLabel("☆좌충우돌☆");
		titleLabel2.setBounds(800, 130, 250, 80);
		titleLabel2.setFont(titleLabel2.getFont().deriveFont(40.0f));
		
		JLabel titleLabel3 = new JLabel("★무인도★");
		titleLabel3.setBounds(800, 210, 250, 80);
		titleLabel3.setFont(titleLabel3.getFont().deriveFont(40.0f));
		
		JLabel titleLabel4 = new JLabel("☆생존일기☆");
		titleLabel4.setBounds(800, 290, 250, 80);
		titleLabel4.setFont(titleLabel4.getFont().deriveFont(40.0f));
		
		JTextField inputNewId = new JTextField(20);
		inputNewId.setBounds(900, 450, 250, 60);
		
		JLabel newIdLabel = new JLabel("아이디");
		newIdLabel.setBounds(800, 450, 250, 60);
		newIdLabel.setFont(newIdLabel.getFont().deriveFont(30.0f));		
		
		JPasswordField inputNewPwd = new JPasswordField(20);
		inputNewPwd.setBounds(900, 520, 250, 60);
		
		JLabel newPwdLabel = new JLabel("비밀번호");
		newPwdLabel.setBounds(769, 520, 250, 60);	
		newPwdLabel.setFont(newPwdLabel.getFont().deriveFont(30.0f));	
		
		JLabel Email = new JLabel("이메일");
		Email.setBounds(800, 590, 250, 60);
		Email.setFont(Email.getFont().deriveFont(30.0f));
		
		JTextField inputNewEmail = new JTextField(30);
		inputNewEmail.setBounds(900, 590, 250, 60);
		
		JLabel cancelLabel = new JLabel("취소");
		cancelLabel.setBounds(1000, 690, 200, 70);
		cancelLabel.setFont(cancelLabel.getFont().deriveFont(40.0f));
		
		 //팝업회색바탕
		JLabel popupBase = new JLabel();
		popupBase.setBounds(340, 0, 550, 700);
//		searchPage.setOpaque(true);
//		searchPage.setBackground(Color.LIGHT_GRAY);
		
		JLabel spEmail1 = new JLabel("e-mail");
		spEmail1.setBounds(380, 95, 200, 80);
		spEmail1.setFont(spEmail1.getFont().deriveFont(25.0f));
		
		JTextField spInputEmail1= new JTextField(30);
		spInputEmail1.setBounds(380, 165, 480, 80);
		
		JLabel searchId = new JLabel("ID 조회");
		searchId.setBounds(555, 270, 200, 55);
		searchId.setFont(searchId.getFont().deriveFont(25.0f));
		
		JLabel spId = new JLabel("ID");
		spId.setBounds(385, 330, 70, 55);
		spId.setFont(spId.getFont().deriveFont(25.0f));
		
		JTextField spInputId = new JTextField();
		spInputId.setBounds(385, 380, 480, 80);
		
		JLabel spEmail2 = new JLabel("e-mail");
		spEmail2.setBounds(385, 470, 150, 65);
		spEmail2.setFont(spEmail2.getFont().deriveFont(25.0f));
		
		JTextField spInputEmail2 = new JTextField();
		spInputEmail2.setBounds(385, 530, 480, 80);
		
		JLabel searchPwd = new JLabel("PW 조회");
		searchPwd.setBounds(555, 650, 200, 55);
		searchPwd.setBackground(Color.WHITE);
		searchPwd.setFont(searchPwd.getFont().deriveFont(25.0f));
		
		Image icon1 = new ImageIcon("image/islandandmen.png").getImage().getScaledInstance(720, 800, 0);
		JLabel iconLabel = new JLabel(new ImageIcon(icon1));
		iconLabel.setBounds(0, 0, 720, 800);
		
		JTextField id = new JTextField(20);
		id.setBounds(900, 480, 250, 70);	
		
		JLabel idLabel = new JLabel("아이디");
		idLabel.setBounds(800, 480, 250, 70);
		idLabel.setFont(idLabel.getFont().deriveFont(30.0f));		
		
		JPasswordField password = new JPasswordField(30);
		password.setBounds(900, 580, 250, 70);		
		
		JLabel pwdLabel = new JLabel("비밀번호");
		pwdLabel.setBounds(769, 580, 250, 70);
		pwdLabel.setFont(pwdLabel.getFont().deriveFont(30.0f));		
		
		JLabel loginLabel = new JLabel("로그인");
		loginLabel.setBounds(850, 640, 170, 70);
		loginLabel.setFont(loginLabel.getFont().deriveFont(40.0f));
		
		JLabel searchLabel = new JLabel("ID/PW찾기");
		searchLabel.setBounds(950, 690, 200, 70);
		searchLabel.setFont(searchLabel.getFont().deriveFont(33.0f));
		
		JLabel createLabel = new JLabel("회원가입");
		createLabel.setBounds(770, 690, 170, 70);
		createLabel.setFont(createLabel.getFont().deriveFont(40.0f));
		
		//새 게임위에 유저 아이디 표시하는 곳
		JLabel userId = new JLabel(id.getText());
		userId.setBounds(900, 400, 230, 70);
		userId.setFont(userId.getFont().deriveFont(30,0f));
		
		JLabel newGame = new JLabel("새 게임");
		newGame.setBounds(920, 490, 180, 70);
		newGame.setFont(newGame.getFont().deriveFont(30,0f));
		
		JLabel loadGame = new JLabel("불러오기");
		loadGame.setBounds(895, 580, 235, 70);
		loadGame.setFont(loadGame.getFont().deriveFont(30,0f));
		
		//조회된 아이디 확인되는 곳
		JLabel checkId = new JLabel();
		checkId.setBounds(385, 360, 480, 80);
		
		JLabel confirm = new JLabel("확인");
		confirm.setBounds(530, 650, 200, 60);
		
		JLabel notRegistUser = new JLabel("등록된 회원이 없습니다.");
		notRegistUser.setBounds(375, 370, 540, 85);
		notRegistUser.setFont(notRegistUser.getFont().deriveFont(20,0f));
		
		JLabel newPassword1 = new JLabel("새 비밀번호");
		newPassword1.setBounds(390, 135, 370, 80);
		newPassword1.setFont(newPassword1.getFont().deriveFont(20,0f));
		
		JPasswordField inputNewPwd1 = new JPasswordField(20);
		inputNewPwd1.setBounds(390, 245, 480, 80);
		
		JLabel sameNewPwd1 = new JLabel("비밀번호 확인");
		sameNewPwd1.setBounds(390, 340, 380, 80);
		sameNewPwd1.setFont(sameNewPwd1.getFont().deriveFont(20,0f));
		
		JPasswordField inputSameNewPwd1 = new JPasswordField(20);
		inputSameNewPwd1.setBounds(390, 420, 480, 80);
		
		JLabel checkIdPwd = new JLabel("아이디/비밀번호를" + "\n확인해주세요");
		checkIdPwd.setBounds(310,350, 465, 130);
		
		/*로그인 클릭*/
		loginLabel.addMouseListener(new MouseListener() {		
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {
				
			String inputId = id.getText();
			String inputPwd = "";
				
				char[] cpwd = password.getPassword();
				for(char gpwd : cpwd) {
					Character.toString(gpwd);
					inputPwd += (inputPwd.equals("")) ? "" + gpwd + "" : "" + gpwd + "";
				}
		
				Map<String, String> userInfo = new HashMap<>();
				userInfo.put("id", inputId);
				userInfo.put("pwd", inputPwd);
		
				LoginController login = new LoginController();
				login.UserLogin(userInfo);
				
				userId.setVisible(true);
				newGame.setVisible(true);
				loadGame.setVisible(true);
				
				id.setVisible(false);
				idLabel.setVisible(false);
				password.setVisible(false);
				pwdLabel.setVisible(false);
				loginLabel.setVisible(false);
				createLabel.setVisible(false);
				searchLabel.setVisible(false);
			}
		});
		/*회원가입 클릭*/
		createLabel.addMouseListener(new MouseListener() {		
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				password.setVisible(false);
				id.setVisible(false);
				idLabel.setVisible(false);
				pwdLabel.setVisible(false);
				searchLabel.setVisible(false);
				loginLabel.setVisible(false);
				
				newPwdLabel.setVisible(true);
				newIdLabel.setVisible(true);
				inputNewId.setVisible(true);
				inputNewPwd.setVisible(true);
				Email.setVisible(true);
				inputNewEmail.setVisible(true);
				cancelLabel.setVisible(true);
				
				String newId = inputNewId.getText();
				String newPwd = "";
				String newEmail = inputNewEmail.getText();
				
				char[] cpwd2 = inputNewPwd.getPassword();
				for(char gpwd2 : cpwd2) {
					Character.toString(gpwd2);
					newPwd += (newPwd.equals("")) ? "" + gpwd2 + "" : "" + gpwd2 + "";
				}

				Map<String, String> newUserInfo = new HashMap<>();
				newUserInfo.put("id", newId);
				newUserInfo.put("pwd", newPwd);
				newUserInfo.put("email", newEmail);	
				
			}
		});
		
		/*ID/PW찾기 클릭*/
		searchLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}			
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
				popupBase.setVisible(true);
				spEmail1.setVisible(true);
				spInputEmail1.setVisible(true);
				searchId.setVisible(true);
				spId.setVisible(true);
				spInputId.setVisible(true);
				spEmail2.setVisible(true);
				spInputEmail2.setVisible(true);
				searchPwd.setVisible(true);
				
				id.setEditable(false);
				password.setEditable(false);
				
				
			}
		});
		/*회원가입에 취소 클릭*/
		cancelLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}			
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
				password.setVisible(true);
				id.setVisible(true);
				idLabel.setVisible(true);
				pwdLabel.setVisible(true);
				searchLabel.setVisible(true);
				loginLabel.setVisible(true);
				
				newPwdLabel.setVisible(false);
				newIdLabel.setVisible(false);
				inputNewId.setVisible(false);
				inputNewPwd.setVisible(false);
				Email.setVisible(false);
				inputNewEmail.setVisible(false);
				cancelLabel.setVisible(false);
			}
		});
		/*ID/PW찾기에 ID조회 클릭*/
		searchId.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				String emailForId = spInputEmail1.getText();

			}
		});
		/*ID/PW찾기에 PW조회 클릭*/
		searchPwd.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}			
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
				String idForPwd = spInputId.getText();
				String emailForPwd = spInputEmail2.getText();

			}

		});
		//아이디조회창에서 확인 클릭
		confirm.addMouseListener(new MouseListener() {			
			@Override
			public void mouseReleased(MouseEvent e) {}			
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			}
		});
		
		panel1.add(checkId);
		panel1.add(confirm);
		panel1.add(notRegistUser);
		panel1.add(newPassword1);
		panel1.add(inputNewPwd1);
		panel1.add(sameNewPwd1);
		panel1.add(inputSameNewPwd1);
		
		checkId.setVisible(false);
		confirm.setVisible(false);
		notRegistUser.setVisible(false);
		newPassword1.setVisible(false);
		inputNewPwd1.setVisible(false);
		sameNewPwd1.setVisible(false);
		inputSameNewPwd1.setVisible(false);
		
		panel1.add(newGame);
		panel1.add(loadGame);
		panel1.add(userId);
		
//		newGame.setVisible(false);
//		loadGame.setVisible(false);
//		userId.setVisible(false);
		
		panel1.add(popupBase);
		panel1.add(spEmail1);
		panel1.add(spInputEmail1);
		panel1.add(searchId);
		panel1.add(spId);
		panel1.add(spInputId);
		panel1.add(spEmail2);
		panel1.add(spInputEmail2);
		panel1.add(searchPwd);
		
		popupBase.setVisible(false);
		spEmail1.setVisible(false);
		spInputEmail1.setVisible(false);
		searchId.setVisible(false);
		spId.setVisible(false);
		spInputId.setVisible(false);
		spEmail2.setVisible(false);
		spInputEmail2.setVisible(false);
		searchPwd.setVisible(false);
		
		panel1.add(cancelLabel);
		panel1.add(Email);
		panel1.add(inputNewEmail);
		panel1.add(newIdLabel);
		panel1.add(inputNewId);
		panel1.add(inputNewPwd);
		panel1.add(newPwdLabel);
		
		cancelLabel.setVisible(false);
		Email.setVisible(false);
		inputNewEmail.setVisible(false);
		newPwdLabel.setVisible(false);
		newIdLabel.setVisible(false);
		inputNewId.setVisible(false);
		inputNewPwd.setVisible(false);
		
		panel1.add(iconLabel);
		panel1.add(id);
		panel1.add(password);
		panel1.add(idLabel);
		panel1.add(pwdLabel);
		panel1.add(loginLabel);
		panel1.add(createLabel);
		panel1.add(searchLabel);
		
		panel1.add(titleLabel1);
		panel1.add(titleLabel2);
		panel1.add(titleLabel3);
		panel1.add(titleLabel4);	
		
		this.add(panel1);
		
		this.setVisible(true);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
}
